//
//  AlertFactory.swift
//  Fonia
//
//  Created by Anton Shcherbach on 24.03.21.
//

import UIKit

class AlertFactory {
	enum Types: CaseIterable {
		case error
		
		var title: String {
			switch self {
			case .error:
				return NSLocalizedString("error_default_title", comment: "")
			}
		}
		
		var actionsTitles: [String] {
			switch self {
			case .error:
				return [NSLocalizedString("error_default_ok_button_title", comment: "")]
			}
		}
	}
	
	static func present(alertType: AlertFactory.Types,
						message: String = "",
						on controller: UIViewController,
						successHandler: AlertHandler? = nil) throws {
		let alertPresenter = AlertPresenter()
		
		alertType.actionsTitles.forEach({
			alertPresenter.addAction(titled: $0,
									 style: .default,
									 handler: successHandler)
		})
		
		try alertPresenter.present(title: alertType.title,
									message: message,
									on: controller)
	}
}

