//
//  VerificationCodeGenerator.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 06/05/2021.
//

import XCTest
@testable import Fonia

class VerificationCodeGeneratorTests: XCTestCase {

	func testCodeGeneration() {
		let code = VerificationCodeGenerator.generateDigitCode(length: 4)
		XCTAssertEqual(code.count, 4)
		XCTAssertNotNil(Int(code), "Generated code should contain only digits")
	}

}
