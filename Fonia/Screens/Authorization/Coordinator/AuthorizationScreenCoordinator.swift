//
//  AuthorizationScreenCoordinator.swift
//  Fonia
//
//  Created by Anton Shcherbach on 20/04/2021.
//

import UIKit

class AuthorizationScreenCoordinator: BaseCoordinator {
	let navigationController: UINavigationController

	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	override func start() {
		navigationController.setNavigationBarHidden(true, animated: true)

		guard let viewController = UIStoryboard(name: "Authorization", bundle: nil).instantiateInitialViewController() as? AuthorizationVC  else { return }
		viewController.delegate = self
		viewController.modalPresentationStyle = .overFullScreen
		navigationController.viewControllers = [viewController]
	}
}

extension AuthorizationScreenCoordinator: AuthorizationScreenDelegate {
	func showErrorScreen() {
		let coordinator = AuthorizationErrorCoordinator(navigationController: navigationController)
		addDependency(coordinator)
		coordinator.start()
		
		coordinator.isCompleted = { [weak self] in
			self?.removeDependency(coordinator)
		}
	}
	
	func showSuccessScreen() {
		let coordinator = AuthorizationSuccessCoordinator(navigationController: navigationController)
		addDependency(coordinator)
		coordinator.start()
		
		coordinator.isCompleted = { [weak self] in
			self?.removeDependency(coordinator)
			
			self?.isCompleted?()
		}
	}
}
