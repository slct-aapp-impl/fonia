//
//  AlertPresenter.swift
//  Fonia
//
//  Created by Anton Shcherbach on 24.03.21.
//

import UIKit

typealias AlertHandler = (UIAlertAction) -> Void

enum AlertPresenterError: Error {
	case attemptToPresentAlertWitchIsAlreadyPresenting
}

class AlertPresenter {
	private var actionHandlers = [UIAlertAction: AlertHandler?]()
	
	func addAction(titled title: String,
				   style: UIAlertAction.Style,
				   handler: AlertHandler?) {
		let action = UIAlertAction(title: title, style: style, handler: handler)
		actionHandlers[action] = handler
	}

	@discardableResult
	func present(title: String,
				 message: String = "",
				 on controller: UIViewController) throws -> UIAlertController {
		let alert = UIAlertController(title: title,
									  message: message,
									  preferredStyle: .alert)
		actionHandlers.keys.forEach({ alert.addAction($0) })
		
		if controller.presentedViewController == nil {
			controller.present(alert, animated: true, completion: nil)
		} else {
			throw AlertPresenterError.attemptToPresentAlertWitchIsAlreadyPresenting
		}
		
		return alert
	}
}


