//
//  PushNotificationPermissionVC.swift
//  Fonia
//
//  Created by Anton Shcherbach on 22.03.21.
//

import UIKit
import AppTrackingTransparency


protocol PushNotificationPermissionDelegate: AnyObject {
	func showAppSettings()
	func permissionsAccepted()
}

class PushNotificationPermissionVC: UIViewController {
	weak var delegate: PushNotificationPermissionDelegate?
	
	private let pushNotificationManager = PushNotificationManager()
	
	private var notificationStatus: PushNotificationManager.Status? {
		didSet {
			guard let notificationStatus = notificationStatus else { return }
			
			DispatchQueue.main.async {
				switch notificationStatus {
				case .disabled, .unknown:
					self.updateUI()
				case .enabled:
					self.delegate?.permissionsAccepted()
				}
			}
		}
	}

	@IBOutlet private weak var actionButton: UIButton! {
		didSet {
			actionButton.layer.cornerRadius = 6.0
			actionButton.isHidden = true
		}
	}

	@IBOutlet private weak var titleLabel: UILabel! {
		didSet {
			titleLabel.isHidden = true
		}
	}
		
	// MARK: - Controller Life Cycle
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		refreshCurrentNotificationStatus()
		NotificationCenter.default.addObserver(self, selector: #selector(willEnterForegroundNotification), name: UIApplication.willEnterForegroundNotification, object: nil)
	}

	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	// MARK: - Helper Methods

	private func refreshCurrentNotificationStatus() {
		pushNotificationManager.getNotificationsStatus { [weak self] (status) in
			guard let strongSelf = self else { return }
			strongSelf.notificationStatus = status
		}
	}
	
	@objc
	private func updateUI() {
		guard let notificationStatus = notificationStatus else { return }
		
		makeUIElementsVisible(elements: [titleLabel, actionButton])
	
		switch notificationStatus {
		case .disabled:
			titleLabel.text = NSLocalizedString("permission_notifications_title", comment: "")
			actionButton.setTitle(NSLocalizedString("permission_notifications_button_go_to_settings", comment: ""), for: .normal)			
		case .unknown:
			titleLabel.text = NSLocalizedString("permission_notifications_title", comment: "")
			actionButton.setTitle(NSLocalizedString("permission_notifications_button_get_access", comment: ""), for: .normal)
		case .enabled:
			break
		}
	}
	
	private func makeUIElementsVisible(elements: [UIView]) {
		elements.forEach({$0.isHidden = false})
	}
	
	// MARK: - Actions
	
	@IBAction func actionButtonTouched(_ sender: Any) {
		guard let notificationStatus = notificationStatus else { return }
		
		switch notificationStatus {
		case .disabled:
			delegate?.showAppSettings()
		case .unknown:
			pushNotificationManager.enableNotifications { [weak self] (status) in
				guard let strongSelf = self else { return }
				strongSelf.notificationStatus = status
			}
		case .enabled:
			delegate?.permissionsAccepted()
		}
	}
	
	// MARK: - Notifications
	
	@objc
	private func willEnterForegroundNotification() {
		refreshCurrentNotificationStatus()
	}
}
