//
//  Constants.swift
//  Fonia
//
//  Created by Anton Shcherbach on 1.03.21.
//

import Foundation

struct Constants {
	struct Services {
		static let adMixerBannerZoneId = "36e4c5fd-bd4a-4919-8c5d-815f99206c87"
		static let appsFlyerDevKey = "u7WFN4T7e9bGpnNGvgMkem"
		static let appsFlyerAppleID = "1567855879"
		
//		#if DEBUG
//			static let oneTrustJsUrl = "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"
//			static let oneTrustMobileAppId = "49fc078f-c2b0-418b-b0ad-d4c647fa2974-test"
//		#else
			static let oneTrustJsUrl = "https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"
			static let oneTrustMobileAppId = "49fc078f-c2b0-418b-b0ad-d4c647fa2974"
//		#endif
	}
	
	struct WebURLs {
		static let mainWebPageURL = URL(string: "https://portal.fonia.app/")!
		static let firstTimeLoginWebPageURL = URL(string: "https://fonia.app/zamow/")!
		static let specialOfferURL = URL(string: "https://fonia.app/specialoffers/")!
		static let surveysURL = URL(string: "https://fonia.app/surveys/")!
		static let customerDataUpdateURL =  URL(string: "https://fonia.app/customerdataupdate/")!
	}
	
	struct Backend {
//		#if DEBUG
//			static let baseURL = URL(string: "https://api-gw-d1gdll2p.ew.gateway.dev")!
//			static let apiKey = "AIzaSyDOGDPbrVVgp7RM2Q3MMqLQJIRaGOEi6cM"
//		#else
			static let baseURL = URL(string: "https://api-gw-6713f03m.ew.gateway.dev")!
			static let apiKey = "AIzaSyAoGXHL2ghkvPXx3esoPLGBPdQNWr3T6BM"
//		#endif

		static let sourceId = "0001"
		static let dvtp     = "02" // Default for iphone '02'
	}
	
	static let authorizationReminderTimeInterval: TimeInterval = 15.0*60.0
}

