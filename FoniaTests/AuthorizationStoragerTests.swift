//
//  AutorizationStoragerTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 22/04/2021.
//

@testable import Fonia
import XCTest

class AuthorizationStoragerTests: XCTestCase {
	var storager: AuthorizationStorager!

	override func setUp() {
		super.setUp()
		
		storager = AuthorizationStorager()
		storager.clean()
	}
	
	override func tearDown() {
		storager = nil
	
		super.tearDown()
	}

	func testDefaultNilValue() {
		XCTAssertNil(storager.getPhoneNumber())
	}
	
	func testStorePhoneNumber() {
		let phoneNumber = "+48000000000"
		storager.store(phoneNumber)
		XCTAssertEqual(phoneNumber, storager.getPhoneNumber())
	}
	
	func testStoragerCleanup() {
		let uid = "+48000000000"
		storager.store(uid)
		storager.clean()
		XCTAssertNotEqual(uid, storager.getPhoneNumber())
		XCTAssertNil(storager.getPhoneNumber())
	}
}
