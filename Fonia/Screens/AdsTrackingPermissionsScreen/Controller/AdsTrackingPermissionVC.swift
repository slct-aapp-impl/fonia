//
//  AdsTrackingVC.swift
//  Fonia
//
//  Created by Anton Shcherbach on 06/04/2021.
//

import UIKit

protocol AdsTrackingPermissionDelegate: AnyObject {
	func showAppSettings()
	func permissionsAccepted()
}

class AdsTrackingPermissionVC: UIViewController {
	weak var delegate: AdsTrackingPermissionDelegate?
	
	private let trackingService = ATTrackingService()
	
	private var status: ATTrackingService.Status? {
		didSet {
			guard let status = status else { return }
			
			DispatchQueue.main.async {
				switch status {
				case .disabled, .unknown:
					self.updateUI()
				case .enabled:
					self.delegate?.permissionsAccepted()
				}
			}
		}
	}

	@IBOutlet private weak var actionButton: UIButton! {
		didSet {
			actionButton.layer.cornerRadius = 6.0
			actionButton.isHidden = true
		}
	}

	@IBOutlet private weak var titleLabel: UILabel! {
		didSet {
			titleLabel.isHidden = true
		}
	}
		
	// MARK: - Controller Life Cycle

	override func viewDidLoad() {
		super.viewDidLoad()
		
		refreshCurrentATTrackingStatus()
		NotificationCenter.default.addObserver(self, selector: #selector(willEnterForegroundNotification), name: UIApplication.willEnterForegroundNotification, object: nil)
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	// MARK: - Helper Methods

	private func refreshCurrentATTrackingStatus() {
		trackingService.getStatus { [weak self] (status) in
			guard let strongSelf = self else { return }
			strongSelf.status = status
		}
	}
	
	@objc
	private func updateUI() {
		guard let status = status else { return }
		
		makeUIElementsVisible(elements: [titleLabel, actionButton])
	
		switch status {
		case .disabled:
			titleLabel.text = NSLocalizedString("permission_att_title", comment: "")
			actionButton.setTitle(NSLocalizedString("permission_att_button_go_to_settings", comment: ""), for: .normal)
		case .unknown:
			titleLabel.text = NSLocalizedString("permission_att_title", comment: "")
			actionButton.setTitle(NSLocalizedString("permission_att_button_get_access", comment: ""), for: .normal)
		case .enabled:
			break
		}
	}
	
	private func makeUIElementsVisible(elements: [UIView]) {
		elements.forEach({$0.isHidden = false})
	}
	
	// MARK: - Actions
	
	@IBAction func actionButtonTouched(_ sender: Any) {
		guard let status = status else { return }
		
		switch status {
		case .disabled:
			delegate?.showAppSettings()
		case .unknown:
			trackingService.enableATTTrackings { [weak self] (status) in
				guard let strongSelf = self else { return }
				strongSelf.status = status
			}
		case .enabled:
			delegate?.permissionsAccepted()
		}
	}
	
	// MARK: - Notifications
	
	@objc
	private func willEnterForegroundNotification() {
		refreshCurrentATTrackingStatus()
	}
}
