//
//  PhoneNumberVerificationRequest.swift
//  Fonia
//
//  Created by Anton Shcherbach on 29/04/2021.
//

import Foundation
import SelectivvSDK

struct PhoneNumberVerificationRequestResponse: Codable {
	let code: Int
	let data: String
}

class PhoneNumberVerificationRequest: BackendAPIRequest {
	let sourceId: String
	let phoneNumber: String
	let verificationCode: String
	let apiKeyHeader: String

	init(sourceId: String, phoneNumber: String, verificationCode: String, apiKeyHeader: String) {
		self.sourceId = sourceId
		self.phoneNumber = phoneNumber
		self.verificationCode = verificationCode
		self.apiKeyHeader = apiKeyHeader
	}
	
	var parameters: [String : Any]? {
		return ["sourceId" : sourceId,
				"dvPNr"    : phoneNumber,
				"vc"       : verificationCode]
	}
	
	var endpoint: String {
		return "/verification"
	}

	var method: NetworkService.Method {
		return .POST
	}
	
	var headers: [String: String]? {
		return ["Content-Type" : "application/json",
				"x-api-key"    : apiKeyHeader]
	}
}
