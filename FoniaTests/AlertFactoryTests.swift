//
//  AlertFactoryTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 24.03.21.
//

@testable import Fonia
import XCTest

class AlertFactoryTests: XCTestCase {
	
	func testAlertFactoryAlertsPresenting() {
		for alertType in AlertFactory.Types.allCases {
			do {
				let mockViewController = UIViewController()
				currentKeyWindow()?.rootViewController = mockViewController

				try AlertFactory.present(alertType: alertType,
										 on: mockViewController)
				
				let alert = try XCTUnwrap(mockViewController.presentedViewController as? UIAlertController)
				XCTAssertEqual(try XCTUnwrap(alert.actions.count), alertType.actionsTitles.count)
				
				for (idx, action) in alert.actions.enumerated() {
					XCTAssertEqual(action.title, alertType.actionsTitles[idx])
				}
				XCTAssertEqual(alert.title, alertType.title)
			} catch {
				XCTFail()
			}
		}
	}
	
	func testAlertTypeError() {
		let type = AlertFactory.Types.error
		XCTAssertEqual(type.title, NSLocalizedString("error_default_title", comment: ""))
		XCTAssertEqual(type.actionsTitles, [NSLocalizedString("error_default_ok_button_title", comment: "")])
	}
	
	
	private func currentKeyWindow() -> UIWindow? {
		let keyWindow = UIApplication.shared.connectedScenes
				.filter({$0.activationState == .foregroundActive})
				.map({$0 as? UIWindowScene})
				.compactMap({$0})
				.first?.windows
				.filter({$0.isKeyWindow}).first
		
		return keyWindow
	}

}
