//
//  AdsRootView.swift
//  Fonia
//
//  Created by Anton Shcherbach on 22.03.21.
//

import UIKit
import SelectivvSDK

// TODO: AdsRootView should be configurable with custom color's

class AdsRootView: UIView {

	private lazy var gradientLayer: CAGradientLayer = {
		let gradientLayer = CAGradientLayer()
		gradientLayer.frame =  CGRect(origin: CGPoint.zero, size: frame.size)
		gradientLayer.colors = [UIColor(red: 217.0/255, green: 184.0/255, blue: 120.0/255, alpha: 1.0).cgColor,
								UIColor(red: 210.0/255, green: 147.0/255, blue: 22.0/255, alpha: 1.0).cgColor,
								UIColor(red: 96.0/255, green: 53.0/255, blue: 4.0/255, alpha: 1.0).cgColor,
								UIColor(red: 217.0/255, green: 184.0/255, blue: 120.0/255, alpha: 1.0).cgColor]
		gradientLayer.mask = gradientShapeLayer
		layer.addSublayer(gradientLayer)
		return gradientLayer
	}()
	
	private lazy var gradientShapeLayer: CAShapeLayer = {
		let gradientShapeLayer = CAShapeLayer()
		gradientShapeLayer.lineWidth = 2.0
		gradientShapeLayer.path = UIBezierPath(rect: bounds).cgPath
		gradientShapeLayer.strokeColor = UIColor.black.cgColor
		gradientShapeLayer.fillColor = UIColor.clear.cgColor
		return gradientShapeLayer
	}()
	
	private(set) lazy var closeButton: UIButton = {
		let closeButton = UIButton(type: .custom)
		closeButton.setImage(UIImage(named: "button_close_ad"), for: .normal)
		addSubview(closeButton)
		return closeButton
	}()
	
	private lazy var logoImageView: UIImageView = {
		let logoImageView = UIImageView(image: UIImage(named: "image_main_ad_logo"))
		addSubview(logoImageView)
		return logoImageView
	}()
	
	private(set) lazy var banner: SelectivvBannerAdView = {
		let banner = SelectivvSDKLib.shared.schedulerBanner()!
		addSubview(banner)
		return banner
	}()
	
	private lazy var activityIndicator: UIActivityIndicatorView = {
		let activityIndicator = UIActivityIndicatorView(style: .medium)
		activityIndicator.color = UIColor.white
		addSubview(activityIndicator)
		return activityIndicator

	}()
	
	// MARK: - Init

	override init(frame: CGRect) {
		super.init(frame: frame)
		
		commonInit()
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		
		commonInit()
		
	}
	
	private func commonInit() {
		backgroundColor = UIColor.white
		
		if let gradientLayer = layer as? CAGradientLayer {
			gradientLayer.colors = [UIColor.darkCharcoal.cgColor,
									UIColor.blueCharcoal.cgColor]
		}
	}
	
	// MARK: - Layout Methods
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		gradientShapeLayer.path = UIBezierPath(rect: bounds).cgPath
		gradientLayer.frame =  CGRect(origin: CGPoint.zero, size: frame.size)
		closeButton.frame = CGRect(x: bounds.width-44.0, y: 0.0, width: 44.0, height: 44.0)
		logoImageView.frame = CGRect(x: 10.0, y: 10.0, width: 50.0, height: 12.0)
		banner.frame = bounds.inset(by: UIEdgeInsets(top: 40.0, left: 10.0, bottom: 10.0, right: 10.0))
		activityIndicator.center = banner.center
	}
	
	// MARK: - Animations
	
	func startActivity() {
		activityIndicator.startAnimating()
		activityIndicator.isHidden = false
	}
	
	func stopActivity() {
		activityIndicator.stopAnimating()
		activityIndicator.isHidden = true
	}
	
	// MARK: - Override Class Methods
	
	override open class var layerClass: AnyClass {
	   return CAGradientLayer.classForCoder()
	}
}
