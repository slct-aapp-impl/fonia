//
//  Date+Extensions.swift
//  Fonia
//
//  Created by Anton Shcherbach on 08/07/2021.
//

import Foundation

extension Date {
	
	/// Returns a Date with the specified amount of components added to the one it is called with
	func add(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date? {
		let components = DateComponents(year: years, month: months, day: days, hour: hours, minute: minutes, second: seconds)
		return Calendar.current.date(byAdding: components, to: self)
	}
	
	/// Returns a Date with the specified amount of components subtracted from the one it is called with
	func subtract(years: Int = 0, months: Int = 0, days: Int = 0, hours: Int = 0, minutes: Int = 0, seconds: Int = 0) -> Date? {
		return add(years: -years, months: -months, days: -days, hours: -hours, minutes: -minutes, seconds: -seconds)
	}
	
}
