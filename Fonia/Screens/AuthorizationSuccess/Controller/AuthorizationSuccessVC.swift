//
//  AuthorizationSuccess.swift
//  Fonia
//
//  Created by Anton Shcherbach on 23/04/2021.
//

import UIKit

protocol AuthorizationSuccessScreenDelegate: AnyObject {
	func didClose()
}

class AuthorizationSuccessVC: UIViewController {
	weak var delegate: AuthorizationSuccessScreenDelegate?
	
	@IBOutlet private weak var titleLabel: UILabel! {
		didSet {
			titleLabel.text = NSLocalizedString("authorization_success_title", comment: "")
		}
	}
	
	private let waitingTime: TimeInterval = 2.0
	private var timer: Timer?
	
	// MARK: - Controller Life Cycle
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		timer = Timer.scheduledTimer(withTimeInterval: waitingTime, repeats: false, block: { (timer) in
			self.delegate?.didClose()
		})
	}
	
	deinit {
		timer?.invalidate()
	}
}
