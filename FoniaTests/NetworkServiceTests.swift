//
//  NetworkServiceTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 29/03/2021.
//

import XCTest
import Promises
@testable import Fonia

class URLSessionDataTaskMock: URLSessionDataTask {
	private let closure: () -> Void

	init(closure: @escaping () -> Void) {
		self.closure = closure
	}

	override func resume() {
		closure()
	}
}

class URLSessionMock: URLSession {
	typealias CompletionHandler = (Data?, URLResponse?, Error?) -> Void

	var data: Data?
	var error: Error?
	var httpResponce: HTTPURLResponse?
	
	override func dataTask(with request: URLRequest,
						   completionHandler: @escaping CompletionHandler) -> URLSessionDataTask {
		let data = self.data
		let error = self.error
		let responce = self.httpResponce
		
		return URLSessionDataTaskMock {
			completionHandler(data, responce, error)
		}
	}
}

class NetworkServiceTests: XCTestCase {
	func testSuccessfulResponse() {
		let url = URL(string: "testURL")!

		let session = URLSessionMock()
		session.httpResponce = HTTPURLResponse(url: url,
											   statusCode: 200,
											   httpVersion: nil,
											   headerFields: nil)
		session.data = Data(base64Encoded: "VGVzdA==")

		let networkService = NetworkService(session: session)
		var result: Any?
		let expectation = self.expectation(description: "Completed")

		networkService.request(url: url, method: .GET).then {
			result = $0
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
		XCTAssertNotNil(result)
	}
	
	func testFailureResponse() {
		let url = URL(string: "testURL")!
		
		let session = URLSessionMock()
		session.httpResponce = HTTPURLResponse(url: url,
											   statusCode: 404,
											   httpVersion: nil,
											   headerFields: nil)
		let networkService = NetworkService(session: session)
		var result: Any?
	
		var error: Error = NSError(domain: "", code: 404, userInfo: nil)
		let expectation = self.expectation(description: "Completed")
		networkService.request(url: url, method: .GET).then {
			result = $0
			expectation.fulfill()
		}.catch {
			error = $0
			expectation.fulfill()
		}
		waitForExpectations(timeout: 5, handler: nil)
		XCTAssertNil(result)
		XCTAssertNotNil(error)
	}
	
	
}
