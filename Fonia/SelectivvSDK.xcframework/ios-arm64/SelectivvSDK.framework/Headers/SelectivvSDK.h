//
//  SelectivvSDK.h
//  SelectivvSDK
//
//  Created by Spyrosoft Solutions S.A. - Anton Shcherbach on 28/07/2021.
//

#import <Foundation/Foundation.h>

//! Project version number for SelectivvSDK.
FOUNDATION_EXPORT double SelectivvSDKVersionNumber;

//! Project version string for SelectivvSDK.
FOUNDATION_EXPORT const unsigned char SelectivvSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SelectivvSDK/PublicHeader.h>


