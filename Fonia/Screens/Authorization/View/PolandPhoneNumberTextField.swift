//
//  PolandPhoneNumberTextField.swift
//  Fonia
//
//  Created by Anton Shcherbach on 23/04/2021.
//

import UIKit
import PhoneNumberKit

class PolandPhoneNumberTextField: PhoneNumberTextField {
	private let prefixBeforeNationalNumber = "+48"
	
	override var defaultRegion: String {
		get {
			return "PL"
		}
		set { }
	}
	
	override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		if range.location < prefixBeforeNationalNumber.count {
			return false
		}
		
		return super.textField(textField, shouldChangeCharactersIn: range, replacementString: string)
	}
}
