//
//  AlertPresenterTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 24.03.21.
//

import XCTest
@testable import Fonia

class AlertPresenterTests: XCTestCase {
	
	func testAlertPresentation() {
		let mockViewController = UIViewController()
		currentKeyWindow()?.rootViewController = mockViewController

		do {
			let alertPresenter = AlertPresenter()
			try alertPresenter.present(title: "", on: mockViewController)
		} catch {
			XCTFail()
		}
	
		XCTAssertTrue(mockViewController.presentedViewController is UIAlertController)
	}
	
	func testDisabledAlertTwicePresentation() {
		let mockViewController = UIViewController()
		currentKeyWindow()?.rootViewController = mockViewController

		do {
			let firstAlertPresenter = AlertPresenter()
			try firstAlertPresenter.present(title: "",
											on: mockViewController)
		} catch {
			XCTFail()
		}
		
		let secondAlertPresenter = AlertPresenter()
		XCTAssertThrowsError(try secondAlertPresenter.present(title: "",
															  on: mockViewController))
	}
	
	func testAlertCorrectTitle() {
		let mockViewController = UIViewController()
		currentKeyWindow()?.rootViewController = mockViewController


		let alertPresenter = AlertPresenter()
		let alertTitle = "Title"
		let alert = try? alertPresenter.present(title: alertTitle,
												on: mockViewController)
		XCTAssertEqual(try XCTUnwrap(alert?.title), alertTitle)
	}
	
	func testAlertCorrectMessage() {
		let mockViewController = UIViewController()
		currentKeyWindow()?.rootViewController = mockViewController


		let alertPresenter = AlertPresenter()
		let alertMessage = "Message"
		let alert = try? alertPresenter.present(title: "",
												message: alertMessage,
												on: mockViewController)
		XCTAssertEqual(try XCTUnwrap(alert?.message), alertMessage)
	}
	
	func testAlertActionAdding() {
		let mockViewController = UIViewController()
		currentKeyWindow()?.rootViewController = mockViewController
		
		
		let alertPresenter = AlertPresenter()
		let actionTitle = "Action"
		alertPresenter.addAction(titled: actionTitle, style: .default, handler: nil)
		let alert = try? alertPresenter.present(title: "",
												on: mockViewController)
		XCTAssertEqual(try XCTUnwrap(alert?.actions.count), 1)
		XCTAssertEqual(try XCTUnwrap(alert?.actions.first?.title), actionTitle)
		XCTAssertEqual(try XCTUnwrap(alert?.actions.first?.style), .default)
	}

	private func currentKeyWindow() -> UIWindow? {
		let keyWindow = UIApplication.shared.connectedScenes
				.filter({$0.activationState == .foregroundActive})
				.map({$0 as? UIWindowScene})
				.compactMap({$0})
				.first?.windows
				.filter({$0.isKeyWindow}).first
		
		return keyWindow
	}
}
