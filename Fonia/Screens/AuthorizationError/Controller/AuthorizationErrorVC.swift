//
//  AuthorizationErrorVC.swift
//  Fonia
//
//  Created by Anton Shcherbach on 23/04/2021.
//

import UIKit

protocol AuthorizationErrorScreenDelegate: AnyObject {
	func didClose()
}

class AuthorizationErrorVC: UIViewController {
	weak var delegate: AuthorizationErrorScreenDelegate?
	
	@IBOutlet private weak var errorLabel: UILabel! {
		didSet {
			errorLabel.text = NSLocalizedString("authorization_error_title", comment: "")
		}
	}
	
	@IBOutlet private weak var backButton: UIButton! {
		didSet {
			backButton.layer.cornerRadius = 6.0
			backButton.setTitle(NSLocalizedString("authorization_error_button_back", comment: ""), for: .normal)
		}
	}
	
	// MARK: - Actions
	
	@IBAction func backButtonTouched(_ sender: Any) {
		delegate?.didClose()
	}
}
