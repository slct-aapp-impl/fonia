//
//  LaunchInstructor.swift
//  Fonia
//
//  Created by Anton Shcherbach on 17/05/2021.
//

import Foundation

class ScreenGateWay {
	enum ScreenType {
		case pushNotificationPermissionScreen
		case atTrackingPermissionScreen
		case authorizationScreen
		case mainScreen
		
		var priority: Int {
			switch self {
			case .atTrackingPermissionScreen:
				return 1000
			case .pushNotificationPermissionScreen:
				return 999
			case .authorizationScreen:
				return 998
			case .mainScreen:
				return 997
			}
		}
	}
	
	private let pushNotificationManager: PushNotificationManager
	private let atTrackingService: ATTrackingService
	private let authorizationStorager: AuthorizationStorager
	
	init(pushNotificationManager: PushNotificationManager = PushNotificationManager(),
		 atTrackingService: ATTrackingService = ATTrackingService(),
		 authorizationStorager: AuthorizationStorager = AuthorizationStorager.default) {
		
		self.pushNotificationManager = pushNotificationManager
		self.atTrackingService = atTrackingService
		self.authorizationStorager = authorizationStorager
	}
	
	func getCurrentNeededScreenType() -> ScreenType {
		var screenTypes: [ScreenType] = []
		
		var pushNotificationStatus: PushNotificationManager.Status?

		let group = DispatchGroup()
		group.enter()
		pushNotificationManager.getNotificationsStatus(status: { (status) in
			pushNotificationStatus = status
			group.leave()
		})

		var atTrackingStatus: ATTrackingService.Status?
		group.enter()
		atTrackingService.getStatus { (status) in
			atTrackingStatus = status
			group.leave()
		}

		group.wait()

		if pushNotificationStatus != .enabled {
			screenTypes.append(.pushNotificationPermissionScreen)
		}

		if atTrackingStatus != .enabled {
			screenTypes.append(.atTrackingPermissionScreen)
		}

		if AuthorizationStorager.default.isUserAuthorized() == false {
			screenTypes.append(.authorizationScreen)
		}

		screenTypes.append(.mainScreen)
			
		return screenTypes.sorted(by: {$0.priority > $1.priority}).first ?? .mainScreen
	}
}
