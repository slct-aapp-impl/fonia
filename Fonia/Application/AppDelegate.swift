//
//  AppDelegate.swift
//  Fonia
//
//  Created by Anton Shcherbach on 1.03.21.
//

import UIKit
import AppTrackingTransparency
import Firebase
import Promises
import AppsFlyerLib
import OTPublishersHeadlessSDK
import SelectivvSDK

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
	var window: UIWindow?
	
	private lazy var applicationCoordinator: Coordinator = AppCoordinator(window: window!)
	
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		UNUserNotificationCenter.current().delegate = self

		configureSelectivvSDK(application: application)
		configureFirebaseSDK()
		configureAppsFlyerSDK()
		configureOTPublisherSDK()
				
		startApp()

		return true
	}
	
	func startApp() {
		applicationCoordinator.start()
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		AppsFlyerLib.shared().start()
		SelectivvSDKLib.shared.didBecomeActive()
	}
	
	func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
		AppsFlyerLib.shared().handlePushNotification(userInfo)
	}
	
	// Open URI-scheme for iOS 9 and above
	func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
		AppsFlyerLib.shared().handleOpen(url, sourceApplication: sourceApplication, withAnnotation: annotation)
		return true
	}
	// Report Push Notification attribution data for re-engagements
	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		AppsFlyerLib.shared().handleOpen(url, options: options)
		return true
	}
	
	// MARK: - SDK configs
	
	private func configureAppsFlyerSDK() {
		AppsFlyerLib.shared().appsFlyerDevKey = Constants.Services.appsFlyerDevKey
		AppsFlyerLib.shared().appleAppID = Constants.Services.appsFlyerAppleID
		
		#if DEBUG
		AppsFlyerLib.shared().isDebug = true
		#endif
	}
	
	public func configureOTPublisherSDK(_ completion: (()->())? = nil) {
		OTPublishersHeadlessSDK.shared.startSDK(storageLocation: "cdn.cookielaw.org",
												domainIdentifier: Constants.Services.oneTrustMobileAppId,
												languageCode: "en",
												params: nil) { response in
			print("status: \(response.status)")
			print("result: \(response.responseString ?? "")")
			print("error: \(response.error)")
			
			completion?()
		}
	}
	
	private func configureFirebaseSDK() {		
		let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
		guard let options = FirebaseOptions(contentsOfFile: firebaseConfig ?? "") else {
			fatalError("Invalid Firebase configuration file.")
		}
		
		FirebaseApp.configure(options: options)
	}
	
	private func configureSelectivvSDK(application: UIApplication) {
		SelectivvSDKLib.initialize(with: SelectivvSDKLibOptions(zoneId: Constants.Services.adMixerBannerZoneId,
																dvtp: Constants.Backend.dvtp,
																sourceId: Constants.Backend.sourceId,
																apiURL: Constants.Backend.baseURL,
																apiKey: Constants.Backend.apiKey,
																notificationTitle: NSLocalizedString("push_notification_ads_reminder_title", comment: ""),
																notificationBody: NSLocalizedString("push_notification_ads_reminder_body", comment: "")),
								   application: application)
	}
}

extension AppDelegate: UNUserNotificationCenterDelegate {
	func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		SelectivvSDKLib.shared.willPresentPushNotification(identifier: notification.request.identifier)
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
		SelectivvSDKLib.shared.didReceivePushNotification(identifier: response.notification.request.identifier)
	}
}
