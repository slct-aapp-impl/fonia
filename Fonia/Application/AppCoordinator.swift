//
//  AppCoordinator.swift
//  Fonia
//
//  Created by Anton Scherbach on 25.03.21.
//

import UIKit
import OTPublishersHeadlessSDK
import MBProgressHUD
import Reachability

protocol OneTrustListenerDelegate: AnyObject {
	func onShowBanner()
}

class OneTrustListener: OTEventListener {
	weak var delegate: OneTrustListenerDelegate?
	
	func onShowBanner() {
		delegate?.onShowBanner()
	}
}

class AppCoordinator: BaseCoordinator {
	let window : UIWindow
	
	private lazy var navigationController: UINavigationController = {
		let navigationController = UINavigationController()
		return navigationController
	}()
	
	private lazy var gateWay: ScreenGateWay = ScreenGateWay()
	
	init(window: UIWindow) {
		self.window = window
		super.init()
		
		window.rootViewController = navigationController
		window.makeKeyAndVisible()
	}
	
	override func start() {
		switch gateWay.getCurrentNeededScreenType() {
		case .mainScreen:
			startMainFlow()
		case .pushNotificationPermissionScreen:
			startPermissionFlow()
		case .atTrackingPermissionScreen:
			startATTrackingFlow()
		case .authorizationScreen:
			startAuthorizationFlow()
		}
		
		// Should always attempt to show one trust
		showOneTrustIfNeeded()
	}
	
	private lazy var reachability: Reachability = {
		let reachability = try! Reachability()
		try? reachability.startNotifier()
		return reachability
	}()
	
	private lazy var listener: OneTrustListener = {
		let listener = OneTrustListener()
		listener.delegate = self
		return listener
	}()
	
	private func startATTrackingFlow() {
		let rootCoordinator = AdsTrackingPermissionScreenCoordinator(navigationController: navigationController)
		addDependency(rootCoordinator)
		rootCoordinator.isCompleted = { [weak self] in
			self?.removeDependency(rootCoordinator)
			self?.start()
		}
		rootCoordinator.start()
	}
	
	private func startPermissionFlow() {
		let rootCoordinator = PushNotificationPermissionScreenCoordinator(navigationController: navigationController)
		addDependency(rootCoordinator)
		rootCoordinator.isCompleted = { [weak self] in
			self?.removeDependency(rootCoordinator)
			self?.start()
		}
		rootCoordinator.start()
	}
	
	private func startMainFlow() {
		let rootCoordinator = MainScreenCoordinator(navigationController: navigationController)
		addDependency(rootCoordinator)
		rootCoordinator.start()
	}
	
	private func startAuthorizationFlow() {
		let rootCoordinator = AuthorizationScreenCoordinator(navigationController: navigationController)
		addDependency(rootCoordinator)
		rootCoordinator.isCompleted = { [weak self] in
			self?.removeDependency(rootCoordinator)
			self?.start()
		}
		rootCoordinator.start()
	}
	
	private func showOneTrustIfNeeded() {
		// 1 if Banner/Preference Center shown
		if OTPublishersHeadlessSDK.shared.isBannerShown() == 1 && OTPublishersHeadlessSDK.shared.shouldShowBanner() == false {
			return
		}
		if reachability.connection == .unavailable {
			presentOneTrustInternetConnectionError()
		} else {
			if OTPublishersHeadlessSDK.shared.isBannerShown() == -1 { // -1 if SDK not initialized yet
				initializeOneTrustSDK()
			} else {
				showOneTrustBanner()
			}
		}
	}
	
	private func initializeOneTrustSDK() {
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
		
		MBProgressHUD.showAdded(to: navigationController.view, animated: true)
		appDelegate.configureOTPublisherSDK { [weak self] in
			guard let strongSelf = self else { return }
			DispatchQueue.main.async {
				strongSelf.showOneTrustIfNeeded()
				MBProgressHUD.hide(for: strongSelf.navigationController.view, animated: true)
			}
		}
	}
	
	private func showOneTrustBanner() {
		if OTPublishersHeadlessSDK.shared.shouldShowBanner() {
			MBProgressHUD.showAdded(to: navigationController.view, animated: true)
			OTPublishersHeadlessSDK.shared.addEventListener(listener)
			OTPublishersHeadlessSDK.shared.setupUI(navigationController, UIType: .banner)
		}
	}
	
	private func presentOneTrustInternetConnectionError() {
		try? AlertFactory.present(alertType: .error,
								  message: NSLocalizedString("error_no_internet_connection", comment: ""),
								  on: navigationController,
								  successHandler: { [weak self] _ in
									self?.showOneTrustIfNeeded()
								  })
	}
}


extension AppCoordinator: OneTrustListenerDelegate {
	func onShowBanner() {
		MBProgressHUD.hide(for: navigationController.view, animated: true)
	}
}
