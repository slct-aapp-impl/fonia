//
//  PhoneNumberValidatorTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 22/04/2021.
//

import XCTest
@testable import Fonia

class PhoneNumberValidatorTests: XCTestCase {

	func testValidPhoneNumberValidation() {
		let validPhoneNumber = "+48123456789"
		let phoneNumberValidator = PhoneNumberValidator()
		XCTAssertTrue(phoneNumberValidator.validate(validPhoneNumber))
	}
	
	func testInvalidPhoneNumberValidation() {
		let validPhoneNumber = "+123 3484"
		let phoneNumberValidator = PhoneNumberValidator()
		XCTAssertFalse(phoneNumberValidator.validate(validPhoneNumber))
	}

}
