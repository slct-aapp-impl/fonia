//
//  Coordinator.swift
//  Fonia
//
//  Created by Anton Shcherbach on 25.03.21.
//

import Foundation

protocol Coordinator : AnyObject {
	var childCoordinators : [Coordinator] { get set }
	func start()
	var isCompleted: (() -> ())? { get set }
}
