//
//  PushNotificationManagerTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 23.03.21.
//

@testable import Fonia
import XCTest

extension UNNotificationSettings {
	static var fakeAuthorizationStatus: UNAuthorizationStatus = .authorized
	
	static func swizzleAuthorizationStatus() {
		let originalMethod = class_getInstanceMethod(self,
													 #selector(getter: authorizationStatus))!
		let swizzledMethod = class_getInstanceMethod(self,
													 #selector(getter: swizzledAuthorizationStatus))!
		
		method_exchangeImplementations(originalMethod, swizzledMethod)
	}
	
	@objc var swizzledAuthorizationStatus: UNAuthorizationStatus {
		return Self.fakeAuthorizationStatus
	}
}

class MockNSCoder: NSCoder {
	var authorizationStatus = UNAuthorizationStatus.authorized.rawValue
	
	override func decodeInt64(forKey key: String) -> Int64 {
		return Int64(authorizationStatus)
	}
	
	override func decodeBool(forKey key: String) -> Bool {
		return true
	}
}

class UserNotificationCenterMock: UserNotificationCenter {
	var settingsCoder = MockNSCoder()

	func getNotificationSettings(completionHandler: @escaping (UNNotificationSettings) -> Void) {
		let settings = UNNotificationSettings(coder: settingsCoder)!
		completionHandler(settings)
	}
	
	// Properties that let us control the outcome of an authorization request.
	var grantAuthorization = false
	var error: Error?

	func requestAuthorization(options: UNAuthorizationOptions,
							  completionHandler: @escaping (Bool, Error?) -> Void) {
		completionHandler(grantAuthorization, error)
	}
}

class PushNotificationManagerTests: XCTestCase {
	func testSuccessfulAuthorization() {
		let center = UserNotificationCenterMock()
		let manager = PushNotificationManager(notificationCenter: center)

		center.grantAuthorization = true

		var status: PushNotificationManager.Status?
		manager.enableNotifications { status = $0 }
		XCTAssertEqual(status, .enabled)
	}
	
	func testUnsuccessfulAuthorization() {
		let center = UserNotificationCenterMock()
		let manager = PushNotificationManager(notificationCenter: center)

		center.grantAuthorization = false

		var status: PushNotificationManager.Status?
		manager.enableNotifications { status = $0 }
		XCTAssertEqual(status, .disabled)
	}
	
	func testNotificationStatusDenied() {
		let center = UserNotificationCenterMock()
		let manager = PushNotificationManager(notificationCenter: center)
		
		center.settingsCoder.authorizationStatus = UNAuthorizationStatus.denied.rawValue
		
		var status: PushNotificationManager.Status?
		manager.getNotificationsStatus { status = $0 }
		XCTAssertEqual(status, .disabled)
	}
	
	func testNotificationStatusNotDetermined() {
		let center = UserNotificationCenterMock()
		let manager = PushNotificationManager(notificationCenter: center)
		
		center.settingsCoder.authorizationStatus = UNAuthorizationStatus.notDetermined.rawValue
		
		var status: PushNotificationManager.Status?
		manager.getNotificationsStatus { status = $0 }
		XCTAssertEqual(status, .unknown)
	}
	
	func testNotificationStatusAuthorized() {
		let center = UserNotificationCenterMock()
		let manager = PushNotificationManager(notificationCenter: center)
		
		center.settingsCoder.authorizationStatus = UNAuthorizationStatus.authorized.rawValue
		
		var status: PushNotificationManager.Status?
		manager.getNotificationsStatus { status = $0 }
		XCTAssertEqual(status, .enabled)
	}
}
