//
//  PushNotificationPermissionScreenCoordinator.swift
//  Fonia
//
//  Created by Anton Scherbach on 25.03.21.
//

import UIKit

class PushNotificationPermissionScreenCoordinator: BaseCoordinator {
	let navigationController: UINavigationController

	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	override func start() {
		navigationController.setNavigationBarHidden(true, animated: true)

		guard let viewController = UIStoryboard(name: "Permissions", bundle: nil).instantiateInitialViewController() as? PushNotificationPermissionVC  else { return }
		viewController.delegate = self
		viewController.modalPresentationStyle = .overFullScreen
		navigationController.pushViewController(viewController, animated: true)
	}
	
	deinit {
		print("\(String(describing: self)) deinit")
	}
}

extension PushNotificationPermissionScreenCoordinator: PushNotificationPermissionDelegate {
	func showAppSettings() {
		UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
	}
	
	func permissionsAccepted() {
		isCompleted?()
	}
}
