//
//  APIDateFormatterTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 12/04/2021.
//

import Foundation
import XCTest
@testable import Fonia

class APIDateFormatterTests: XCTestCase {

	func testApiDateFormatter() {
		let formatter = SelectivvFailedAdsStoragerDateFormatter()
		let date = Date(timeIntervalSince1970: 1617969600) // 2021-04-09 12:00:00
		let string = formatter.format(date)
		XCTAssertEqual(string, "2021-04-09 14:00:00")
	}
	
	func testFailedApiDateFormatter() {
		let formatter = SelectivvFailedAdsStoragerDateFormatter()
		let date = Date(timeIntervalSince1970: 0)
		let string = formatter.format(date)
		XCTAssertNotEqual(string, "2021-04-09 14:00:00")
	}
	
}
