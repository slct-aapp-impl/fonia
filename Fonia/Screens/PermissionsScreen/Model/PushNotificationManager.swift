//
//  Permissions.swift
//  Fonia
//
//  Created by Anton Shcherbach on 22.03.21.
//

import UIKit

protocol UserNotificationCenter {
	func requestAuthorization(options: UNAuthorizationOptions,
							  completionHandler: @escaping (Bool, Error?) -> Void)
	
	func getNotificationSettings(completionHandler: @escaping (UNNotificationSettings) -> Void)
}

extension UNUserNotificationCenter: UserNotificationCenter {}

class PushNotificationManager {
	enum Status {
		case enabled, disabled, unknown
	}
	
	private let notificationCenter: UserNotificationCenter
	
	init(notificationCenter: UserNotificationCenter = UNUserNotificationCenter.current()) {
		self.notificationCenter = notificationCenter
	}
	
	func getNotificationsStatus(status: @escaping ((PushNotificationManager.Status) -> Void)) {
		notificationCenter.getNotificationSettings { (settings) in
			switch settings.authorizationStatus {
			case .denied:
				status(.disabled)
			case .notDetermined:
				status(.unknown)
			default:
				status(.enabled)
			}
		}
	}
	
	func enableNotifications(options: UNAuthorizationOptions = [.alert, .badge, .sound],
							 status: @escaping ((PushNotificationManager.Status) -> Void)) {
		notificationCenter.requestAuthorization(options: [.alert, .badge, .sound]) { success, error in
			status(success ? .enabled : .disabled)
		}
	}
}

