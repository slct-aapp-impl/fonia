//
//  AuthorizationPresenterTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 23/04/2021.
//

import XCTest
@testable import Fonia

class AuthorizationPresenterMockView: AuthorizationViewProtocol {
	var viewState: AuthorizationViewState?
	var isUILocked: Bool = false
	var isLoginSuccess: Bool = false
	var openInAppWebURL: URL?
	var error: Error?
	
	func setAuthorizationViewState(_ state: AuthorizationViewState) {
		viewState = state
	}
	
	func disableUI(_ disable: Bool) {
		isUILocked = disable
	}
	
	func reactOn(error: Error) {
		isLoginSuccess = false
		
		self.error = error
	}
	
	func success(_ phoneNumber: String) {
		isLoginSuccess = true
	}
	
	func openInAppWebURL(_ url: URL) {
		openInAppWebURL = url
	}
}

class AuthorizationPresenterTests: XCTestCase {
	
	var authorizationPresenterMockView = AuthorizationPresenterMockView()

	func testSuccessAuthorizationFlow() throws {
		let authorizationPresenter = AuthorizationPresenter(authorizationModel: nil,
															viewController: authorizationPresenterMockView,
															state: .verifyPhoneNumber)
		XCTAssertEqual(authorizationPresenter.state, .verifyPhoneNumber)
		XCTAssertEqual(authorizationPresenterMockView.viewState, authorizationPresenter.state)
		authorizationPresenter.setTextFieldValue("+48754000123")
		XCTAssertNotNil(authorizationPresenter.authorizationModel)
		authorizationPresenter.actionForCurrentState()
		XCTAssertTrue(authorizationPresenterMockView.isUILocked)
		wait(for: 2.0)
		XCTAssertFalse(authorizationPresenterMockView.isUILocked)
		XCTAssertEqual(authorizationPresenter.state, .verifySMSCode)
		let authorizationModel = try XCTUnwrap(authorizationPresenter.authorizationModel)
		authorizationPresenter.setTextFieldValue(authorizationModel.expectedCode)
		XCTAssertEqual(authorizationModel.expectedCode,
					   authorizationPresenter.authorizationCode)
		authorizationPresenter.actionForCurrentState()
		wait(for: 2.0)
		XCTAssertTrue(authorizationPresenterMockView.isLoginSuccess)
	}
	
	func testWrongPhoneNumberAuthorizationFlow() {
		let authorizationPresenter = AuthorizationPresenter(authorizationModel: nil,
															viewController: authorizationPresenterMockView,
															state: .verifyPhoneNumber)

		XCTAssertEqual(authorizationPresenter.state, .verifyPhoneNumber)
		XCTAssertEqual(authorizationPresenterMockView.viewState, authorizationPresenter.state)
		authorizationPresenter.setTextFieldValue("+48123")
		XCTAssertNil(authorizationPresenter.authorizationModel)
		authorizationPresenter.actionForCurrentState()
		XCTAssertEqual(try XCTUnwrap(authorizationPresenterMockView.error as? AuthorizationError),
					   AuthorizationError.invalidPhoneNumber)
		wait(for: 0.1)
		XCTAssertEqual(authorizationPresenter.state, .verifyPhoneNumber)
		XCTAssertFalse(authorizationPresenterMockView.isLoginSuccess)
	}

	func testWrongCodeAuthorizationFlow() {
		let authorizationPresenter = AuthorizationPresenter(authorizationModel: nil,
															viewController: authorizationPresenterMockView,
															state: .verifyPhoneNumber)

		XCTAssertEqual(authorizationPresenter.state, .verifyPhoneNumber)
		XCTAssertEqual(authorizationPresenterMockView.viewState, .verifyPhoneNumber)
		authorizationPresenter.setTextFieldValue("+48754000123")
		XCTAssertNotNil(authorizationPresenter.authorizationModel)
		authorizationPresenter.actionForCurrentState()
		XCTAssertTrue(authorizationPresenterMockView.isUILocked)
		wait(for: 2.0)
		XCTAssertFalse(authorizationPresenterMockView.isUILocked)
		XCTAssertEqual(authorizationPresenter.state, .verifySMSCode)
		XCTAssertEqual(authorizationPresenterMockView.viewState, authorizationPresenter.state)
		authorizationPresenter.setTextFieldValue("023456")
		XCTAssertNotEqual(try XCTUnwrap(authorizationPresenter.authorizationModel?.expectedCode),
					   authorizationPresenter.authorizationCode)
		authorizationPresenter.actionForCurrentState()
		XCTAssertEqual(try XCTUnwrap(authorizationPresenterMockView.error as? AuthorizationError),
					   AuthorizationError.invalidCode)
		XCTAssertFalse(authorizationPresenterMockView.isLoginSuccess)
	}
	
	func testBackAuthorizationFlow() {
		let authorizationPresenter = AuthorizationPresenter(authorizationModel: nil,
															viewController: authorizationPresenterMockView,
															state: .verifyPhoneNumber)

		XCTAssertEqual(authorizationPresenter.state, .verifyPhoneNumber)
		XCTAssertEqual(authorizationPresenterMockView.viewState, .verifyPhoneNumber)
		authorizationPresenter.setTextFieldValue("+48754000123")
		XCTAssertNotNil(authorizationPresenter.authorizationModel)
		authorizationPresenter.actionForCurrentState()
		XCTAssertTrue(authorizationPresenterMockView.isUILocked)
		wait(for: 2.0)
		XCTAssertFalse(authorizationPresenterMockView.isUILocked)
		XCTAssertEqual(authorizationPresenter.state, .verifySMSCode)
		authorizationPresenter.backActionForCurrentState()
		XCTAssertEqual(authorizationPresenter.state, .verifyPhoneNumber)
		XCTAssertNil(authorizationPresenter.authorizationModel)
		XCTAssertNil(authorizationPresenter.authorizationCode)
	}
}


extension XCTestCase {

  func wait(for duration: TimeInterval) {
	let waitExpectation = expectation(description: "Waiting")

	let when = DispatchTime.now() + duration
	DispatchQueue.main.asyncAfter(deadline: when) {
	  waitExpectation.fulfill()
	}

	// We use a buffer here to avoid flakiness with Timer on CI
	waitForExpectations(timeout: duration + 1.0)
  }
}
