//
//  AuthorizationSuccessCoordinator.swift
//  Fonia
//
//  Created by Anton Shcherbach on 23/04/2021.
//

import UIKit

class AuthorizationSuccessCoordinator: BaseCoordinator {
	let navigationController: UINavigationController

	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	override func start() {
		guard let viewController = UIStoryboard(name: "AuhorizationSuccess", bundle: nil).instantiateInitialViewController() as? AuthorizationSuccessVC  else { return }
		viewController.modalPresentationStyle = .overFullScreen
		viewController.delegate = self
		navigationController.present(viewController, animated: true, completion: nil)
	}
}

extension AuthorizationSuccessCoordinator: AuthorizationSuccessScreenDelegate {
	func didClose() {
		isCompleted?()
		
		navigationController.dismiss(animated: true, completion: nil)
	}
}
