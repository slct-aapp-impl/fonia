//
//  ButtonStyles.swift
//  Fonia
//
//  Created by Anton Shcherbach on 16/04/2021.
//

import UIKit

private struct ButtonStyleUX {
	static let ButtonFont = UIFont(name: "Roboto-Black", size: 15.0)
	static let TextColor = UIColor.white
	static let BackgroundColor = UIColor.darkCharcoal
	static let CornerRadius: CGFloat = 6.0
}

enum ButtonStyles {
	case sales
	case form
	case profile
	case portal
	
	var style: ButtonStyle {
		switch self {
		case .sales:
			return ButtonStyle(cornerRadius: ButtonStyleUX.CornerRadius,
							   backgroundColor: ButtonStyleUX.BackgroundColor,
							   textColor: ButtonStyleUX.TextColor,
							   text: NSLocalizedString("main_button_sales", comment: ""),
							   font: ButtonStyleUX.ButtonFont,
							   leftIcon: UIImage(named: "icon_sales"),
							   rightIcon: UIImage(named: "icon_arrow"))
		case .form:
			return ButtonStyle(cornerRadius: ButtonStyleUX.CornerRadius,
							   backgroundColor: ButtonStyleUX.BackgroundColor,
							   textColor: UIColor.white,
							   text: NSLocalizedString("main_button_fill_form", comment: ""),
							   font: ButtonStyleUX.ButtonFont,
							   leftIcon: UIImage(named: "icon_edit_anket"),
							   rightIcon: UIImage(named: "icon_arrow"))
		case .profile:
			return ButtonStyle(cornerRadius: ButtonStyleUX.CornerRadius,
							   backgroundColor: ButtonStyleUX.BackgroundColor,
							   textColor: ButtonStyleUX.TextColor,
							   text: NSLocalizedString("main_button_profile", comment: ""),
							   font: ButtonStyleUX.ButtonFont,
							   leftIcon: UIImage(named: "icon_edit_profile"),
							   rightIcon: UIImage(named: "icon_arrow"))
		case .portal:
			return ButtonStyle(cornerRadius: ButtonStyleUX.CornerRadius,
							   backgroundColor: ButtonStyleUX.BackgroundColor,
							   textColor: ButtonStyleUX.TextColor,
							   text: NSLocalizedString("main_button_portal", comment: ""),
							   font: ButtonStyleUX.ButtonFont,
							   leftIcon: UIImage(named: "icon_portal"),
							   rightIcon: UIImage(named: "icon_arrow"))
		}
	}
}
