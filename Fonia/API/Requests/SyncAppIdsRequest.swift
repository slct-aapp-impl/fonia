//
//  SyncAppIdsRequest.swift
//  Fonia
//
//  Created by Anton Shcherbach on 07/07/2021.
//

import Foundation
import SelectivvSDK

struct SyncAppIdsRequestResponse: Codable {
	let code: Int
	let data: String
}

class SyncAppIdsRequest: BackendAPIRequest {
	let sourceId: String
	let dvtp: String
	let phoneNumber: String
	let idfa: String
	let apiKeyHeader: String

	init(sourceId: String, dvtp: String, phoneNumber: String, idfa: String, apiKeyHeader: String) {
		self.sourceId = sourceId
		self.dvtp = dvtp
		self.phoneNumber = phoneNumber
		self.idfa = idfa
		self.apiKeyHeader = apiKeyHeader
	}
	
	var parameters: [String : Any]? {
		return ["sourceId" : sourceId,
				"dvtp"     : dvtp,
				"dvPNr"    : phoneNumber,
				"dvAID"    : idfa]
	}
	
	var endpoint: String {
		return "/ids"
	}
	
	var method: NetworkService.Method {
		return .POST
	}
	
	var headers: [String: String]? {
		return ["Content-Type" : "application/json",
				"x-api-key"    : apiKeyHeader]
	}
}
