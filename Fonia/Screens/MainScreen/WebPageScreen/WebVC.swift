//
//  WebVC.swift
//  Fonia
//
//  Created by Anton Shcherbach on 14/06/2021.
//

import UIKit
import WebKit

class WebVC: UIViewController {
	var url: URL?

	private lazy var webView: WKWebView = {
		let webView = WKWebView()
		view.addSubview(webView)

		webView.translatesAutoresizingMaskIntoConstraints = false
		webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
		webView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
		webView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0).isActive = true
		webView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true

		return webView
	}()

	// MARK: - Controller Life Cycle

	override func viewDidLoad() {
		super.viewDidLoad()

		configureBarButtonItems()
		openCurrentUrl()
	}

	// MARK: - Actions

	@objc
	private func closeButtonTouched() {
		dismiss(animated: true, completion: nil)
	}

	// MARK: - Helper Methods
	
	private func configureBarButtonItems() {
		navigationItem.leftBarButtonItem = UIBarButtonItem(title: NSLocalizedString("web_page_screen_close", comment: ""),
														   style: .done,
														   target: self,
														   action: #selector(closeButtonTouched))

	}

	private func openCurrentUrl() {
		guard let url = url else { return }
		
		let request = URLRequest(url: url)
		webView.load(request)
	}
	
	// MARK: - Presentation Static Method
	
	static func present(with url: URL, title: String? = nil, from controller: UIViewController) {
		let webVC = WebVC()
		webVC.title = title
		webVC.url = url
		
		let nvc = UINavigationController(rootViewController: webVC)
		controller.present(nvc, animated: true, completion: nil)
	}
}
