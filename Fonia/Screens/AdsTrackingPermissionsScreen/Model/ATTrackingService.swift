//
//  ATTTrackingManager.swift
//  Fonia
//
//  Created by Anton Shcherbach on 06/04/2021.
//

import Foundation
import AppTrackingTransparency
import AdSupport
import Promises

protocol ASIdentifierProtocol {
	var advertisingIdentifier: UUID { get }
}

extension ASIdentifierManager: ASIdentifierProtocol {}

enum ATTrackingServiceError: Error {
	case idfaEmpty
}

extension ATTrackingServiceError: LocalizedError {
	public var errorDescription: String? {
		switch self {
		case .idfaEmpty:
			return NSLocalizedString("error_idfa_empty", comment: "")
		}
	}
}

class ATTrackingService {
	enum Status {
		case enabled, disabled, unknown
	}
	
	func getStatus(status: @escaping ((ATTrackingService.Status) -> Void)) {
		if #available(iOS 14, *) {
			switch ATTrackingManager.trackingAuthorizationStatus {
			case .denied:
				status(.disabled)
			case .notDetermined:
				status(.unknown)
			default:
				status(.enabled)
			}
		} else {
			status(.enabled)
		}
	}
	
	func enableATTTrackings(status: @escaping ((ATTrackingService.Status) -> Void)) {
		if #available(iOS 14, *) {
			ATTrackingManager.requestTrackingAuthorization { (requestTrackingAuthorizationStatus) in
				switch requestTrackingAuthorizationStatus {
				case .denied:
					status(.disabled)
				case .notDetermined:
					status(.unknown)
				default:
					status(.enabled)
				}
			}
		} else {
			status(.enabled)
		}
	}
	
	func getIDFA(_ identifierManager: ASIdentifierProtocol = ASIdentifierManager.shared()) -> Promise<String> {
		return Promise<String> { fulfill, reject in
			self.getStatus { status in
				if status == .enabled {
					fulfill(identifierManager.advertisingIdentifier.uuidString)
				} else {
					reject(ATTrackingServiceError.idfaEmpty)
				}
			}
		}
	}
}
