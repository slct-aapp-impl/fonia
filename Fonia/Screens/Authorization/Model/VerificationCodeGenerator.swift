//
//  VerificationCodeGenerator.swift
//  Fonia
//
//  Created by Anton Shcherbach on 06/05/2021.
//

import Foundation

class VerificationCodeGenerator {
	static func generateDigitCode(length: Int) -> String {
		var number = String()
		for _ in 1...length {
		   number += "\(Int.random(in: 0...9))"
		}
		return number
	}
}
