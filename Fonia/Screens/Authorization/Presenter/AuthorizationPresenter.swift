//
//  AuthorizationPresenter.swift
//  Fonia
//
//  Created by Anton Shcherbach on 22/04/2021.
//

import Foundation
import Promises
import SelectivvSDK

struct AuthorizationModel {
	let phoneNumber: String
	let expectedCode: String
	
	init(phoneNumber: String) {
		self.phoneNumber = phoneNumber
		self.expectedCode = VerificationCodeGenerator.generateDigitCode(length: 4)
	}
}

enum AuthorizationError: Error {
	case invalidPhoneNumber
	case invalidCode
	case emptyAuthorizationModel
	
	var localizedDescription: String {
		switch self {
		case .invalidPhoneNumber:
			return NSLocalizedString("authorization_error_invalid_phone_number", comment: "")
		case .invalidCode:
			return NSLocalizedString("authorization_error_invalid_code", comment: "")
		case .emptyAuthorizationModel:
			return NSLocalizedString("authorization_error_unknown", comment: "")
		}
	}
}


enum AuthorizationViewState {
	case verifyPhoneNumber
	case verifySMSCode
}

protocol AuthorizationViewProtocol: AnyObject {
	func setAuthorizationViewState(_ state: AuthorizationViewState)
	func disableUI(_ disable: Bool)
	func reactOn(error: Error)
	func success(_ phoneNumber: String)
	func openInAppWebURL(_ url: URL)
}

class AuthorizationPresenter {
	private(set) var state: AuthorizationViewState {
		didSet {
			view.setAuthorizationViewState(state)
		}
	}
	
	private(set) var authorizationModel: AuthorizationModel? { // TODO
		didSet {
			#if DEBUG
			if let expectedCode = authorizationModel?.expectedCode {
				print("authorizationModel.expectedCode = \(expectedCode)")
			}
			#endif
		}
	}
	private(set) var authorizationCode: String? // TODO:

	weak var view: AuthorizationViewProtocol!
	
	private let phoneNumberValidator = PhoneNumberValidator()
	
	init(authorizationModel: AuthorizationModel?,
		 viewController: AuthorizationViewProtocol,
		 state: AuthorizationViewState) {
		self.authorizationModel = authorizationModel
		self.view = viewController
		self.state = state

		defer {
			self.state = state
		}
	}
	
	func actionForCurrentState() {
		switch state {
		case .verifyPhoneNumber:
			receivePhoneVerificationCode()
		case .verifySMSCode:
			verifySMSCode()
		}
	}
	
	func additionalActionForCurrentState() {
		switch state {
		case .verifyPhoneNumber:
			view.openInAppWebURL(Constants.WebURLs.firstTimeLoginWebPageURL)
		case .verifySMSCode:
			receivePhoneVerificationCode()
		}
	}
	
	func backActionForCurrentState() {
		switch state {
		case .verifyPhoneNumber:
			break
		case .verifySMSCode:
			authorizationModel = nil
			authorizationCode = nil
			
			state = .verifyPhoneNumber
		}
	}
	
	func setTextFieldValue(_ text: String) {
		switch state {
		case .verifyPhoneNumber:
			if validateInput(phoneNumber: text) == nil {
				authorizationModel = AuthorizationModel(phoneNumber: text)
			} else {
				authorizationModel = nil
			}
		case .verifySMSCode:
			authorizationCode = text
		}
	}
	
	func reset() {
		authorizationModel = nil
		authorizationCode = nil
		state = .verifyPhoneNumber
	}
	
	private func receivePhoneVerificationCode() {
		guard let authorizationModel = authorizationModel else {
			view.reactOn(error: AuthorizationError.invalidPhoneNumber)
			return
		}

		// authorizationModel != nil == valid phone number
		
		if let error = validateInput(phoneNumber: authorizationModel.phoneNumber) {
			view.reactOn(error: error)
		} else {
			view.disableUI(true)
			self.sendVerificationCode().then { result in
				self.state = .verifySMSCode
			}.catch { (error) in
				self.view.reactOn(error: error)
			}.always {
				self.view.disableUI(false)
			}
		}
	}
	
	
	private func verifySMSCode() {
		guard let authorizationModel = authorizationModel else {
			view.reactOn(error: AuthorizationError.invalidPhoneNumber)
			return
		}

		if let error = validateCode(code: authorizationCode ?? "",
									expectedCode: authorizationModel.expectedCode) {
			view.reactOn(error: error)
		} else {
			syncAppIds(with: authorizationModel.phoneNumber)
		}
	}
	
	private func syncAppIds(with phoneNumber: String) {
		view.disableUI(true)
		sendSyncAppIdsRequest(with: phoneNumber).then { _ in
			self.view.success(phoneNumber)
		}.catch { error in
			self.view.reactOn(error: error)
		}.always {
			self.view.disableUI(false)
		}
	}
	
	private func sendSyncAppIdsRequest(with phoneNumber: String) -> Promise<Bool> {
		return Promise<Bool> { fulfill, reject in
			let attTrackingService = ATTrackingService()
			attTrackingService.getIDFA().then { idfa in
				let request = SyncAppIdsRequest(sourceId: Constants.Backend.sourceId,
												dvtp: Constants.Backend.dvtp,
												phoneNumber: phoneNumber,
												idfa: idfa,
												apiKeyHeader: Constants.Backend.apiKey)
				
				
				SelectivvSDKLib.shared.backendService.request(request: request, requestResponceType: SyncAppIdsRequestResponse.self).then { result in
					fulfill(true)
				}
			}.catch(reject)
		}
	}
	
	private func sendVerificationCode() -> Promise<Bool> {
		return Promise<Bool> { fulfill, reject in
			guard let authorizationModel = self.authorizationModel else {
				reject(AuthorizationError.invalidPhoneNumber)
				return
			}

			let request = PhoneNumberVerificationRequest(sourceId         : Constants.Backend.sourceId,
														 phoneNumber      : authorizationModel.phoneNumber,
														 verificationCode : authorizationModel.expectedCode,
														 apiKeyHeader     : Constants.Backend.apiKey)
			SelectivvSDKLib.shared.backendService.request(request: request,
										  requestResponceType: PhoneNumberVerificationRequestResponse.self).then { result in
											fulfill(true)
										}.catch(reject)
		}
	}
	
	private func validateInput(phoneNumber: String) -> AuthorizationError? {
		if !phoneNumberValidator.validate(phoneNumber) {
			return .invalidPhoneNumber
		}
		return nil
	}
	
	private func validateCode(code: String, expectedCode: String) -> AuthorizationError? {
		if code.isEmpty || expectedCode != code {
			return .invalidCode
		}
		return nil
	}

}

