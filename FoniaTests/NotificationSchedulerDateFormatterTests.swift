//
//  NotificationSchedulerDateFormatterTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 06/05/2021.
//

import XCTest
@testable import Fonia

class NotificationSchedulerDateFormatterTests: XCTestCase {
	var notificationSchedulerDateFormatter: NotificationSchedulerDateFormatter!
	
	override func setUp() {
		super.setUp()
	
		notificationSchedulerDateFormatter = NotificationSchedulerDateFormatter()
	}
	
	override func tearDown() {
		super.tearDown()
		
		notificationSchedulerDateFormatter = nil
	}

	func testFormatterWithValidAPIString() {
		let time = "08:00"
		let date = notificationSchedulerDateFormatter.formatToCurrentDateWith(time)
		do {
			let components = Calendar.current.dateComponents([.hour, .minute],
															 from: try XCTUnwrap(date))
			
			XCTAssertEqual(try XCTUnwrap(components.hour), 8)
			XCTAssertEqual(try XCTUnwrap(components.minute), 0)
		} catch {
			XCTFail()
		}
	}
	
	func testFormatterWithInvalidAPIString() {
		let time = ":00"
		let date = notificationSchedulerDateFormatter.formatToCurrentDateWith(time)
		XCTAssertNil(date)
	}
	
	func testFormatterWithWrongAPIStringFormat() {
		let time = "06:25 PM"
		let date = notificationSchedulerDateFormatter.formatToCurrentDateWith(time)
		XCTAssertNil(date)
	}
}
