//
//  ViewController.swift
//  Fonia
//
//  Created by Anton Shcherbach on 1.03.21.
//

import UIKit
import UserNotifications
import WebKit
import SelectivvSDK

class MainVC: UIViewController {
	@IBOutlet private weak var webView: WKWebView! {
		didSet {
			let myRequest = URLRequest(url: Constants.WebURLs.mainWebPageURL)
			webView.load(myRequest)
		}
	}
	
	@IBOutlet private weak var adsContainerView: UIView! {
		didSet {
			adsContainerView.alpha = 0.0
		}
	}
	
	@IBOutlet private weak var adsRootView: AdsRootView! {
		didSet {
			adsRootView.closeButton.addTarget(self, action: #selector(closeBannerButtonTouched), for: .touchUpInside)
			
			adsRootView.startActivity()
			
			adsRootView.banner.autoRefreshInterval = 0.0
			adsRootView.banner.rootViewController = self
			adsRootView.banner.delegate = self
		}
	}
	@IBOutlet private weak var salesButton: StylizedButton! {
		didSet {
			salesButton.setButtonStyle(ButtonStyles.sales.style)
		}
	}
	
	@IBOutlet private weak var profileButton: StylizedButton! {
		didSet {
			profileButton.setButtonStyle(ButtonStyles.profile.style)
		}
	}
	
	@IBOutlet private weak var editProfileButton: StylizedButton! {
		didSet {
			editProfileButton.setButtonStyle(ButtonStyles.form.style)
		}
	}
	
	@IBOutlet private weak var customerPortalButton: StylizedButton! {
		didSet {
			customerPortalButton.setButtonStyle(ButtonStyles.portal.style)
		}
	}
	
	// MARK: - Controller Life Cycle
	
	override func viewDidLoad() {
		super.viewDidLoad()

		fetchAndStartSchedulerAdsNotificationIfNeeded()
 	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		presentAdsContainerIfNeeded()
	}
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	// MARK: - Helper Methods
	
	private func presentAdsContainerIfNeeded() {
		DispatchQueue.main.async {
			if SelectivvSDKLib.shared.applicationEventHandler.isSchedulerPushNotificationReceived {
				self.adsRootView.banner.loadAd()
				self.showAdsContainerViewAnimated()
			}
		}
	}
	
	private func fetchAndStartSchedulerAdsNotificationIfNeeded() {
		SelectivvSDKLib.shared.applicationEventHandler.delegate = self
		SelectivvSDKLib.shared.fetchRemoteConfigurationAndActivateScheduler(completionHandler: nil)
	}
	
	private func openURL(_ url: URL) {
		let myRequest = URLRequest(url: url)
		webView.load(myRequest)
	}
	
	// MARK: - Ads Showing Methods
	
	private func showAdsContainerViewAnimated(completion: (()->())? = nil) {
		UIView.animate(withDuration: 0.3, animations: {
			self.adsContainerView.alpha = 1.0
		}) { _ in
			completion?()
		}
	}
	
	private func hideAdsContainerViewAnimated() {
		UIView.animate(withDuration: 0.3, animations: {
			self.adsContainerView.alpha = 0.0
		}, completion: nil)
	}
	
	private func forceCloseApp() {
		self.view.addSubview(view) // TODO: customer desire
	}
	
	// MARK: - Actions
	
	@objc
	private func closeBannerButtonTouched() {
		forceCloseApp()
	}
	
	@IBAction func salesButtonTouched(_ sender: Any) {
		hideAdsContainerViewAnimated()
		
		openURL(Constants.WebURLs.specialOfferURL)
	}
	
	@IBAction func profileButtonTouched(_ sender: Any) {
		hideAdsContainerViewAnimated()
		
		openURL(Constants.WebURLs.surveysURL)
	}

	@IBAction func editProfileButtonTouched(_ sender: Any) {
		hideAdsContainerViewAnimated()
		
		openURL(Constants.WebURLs.customerDataUpdateURL)
	}
	
	@IBAction func customerPortalButtonTouched(_ sender: Any) {
		hideAdsContainerViewAnimated()
		
		openURL(Constants.WebURLs.mainWebPageURL)
	}
	
	// MARK: - Notifications
	
	@objc
	private func didReceivePushNotification() {
		presentAdsContainerIfNeeded()
	}
}

extension MainVC: SelectivvAdDelegate {
	func adDidReceiveAd(_ ad: Any) {
		adsRootView.stopActivity()
	}
	
	func ad(_ ad: Any, requestFailedWithError error: Error) {
		try? AlertFactory.present(alertType: .error, message: NSLocalizedString("error_ad_failed_to_load", comment: ""), on: self)
		
		adsRootView.stopActivity()
	}
}

extension MainVC: SelectivvSDKAppEventHandlerDelegate {
	func schedulerPushNotificationFlagDidChange() {
		presentAdsContainerIfNeeded()
	}
}
