//
//  PhoneNumberValidator.swift
//  Fonia
//
//  Created by Anton Shcherbach on 22/04/2021.
//

import Foundation
import PhoneNumberKit

class PhoneNumberValidator {
	private let phoneNumberKit = PhoneNumberKit()

	func validate(_ phoneNumber: String) -> Bool {
		return phoneNumberKit.isValidPhoneNumber(phoneNumber)
	}
}
