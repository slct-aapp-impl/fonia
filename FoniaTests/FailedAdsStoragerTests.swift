//
//  FailedAdsStoragerTests.swift
//  FoniaTests
//
//  Created by Anton Shcherbach on 09/04/2021.
//

import Foundation
import XCTest
@testable import Fonia

class FailedAdsStoragerTests: XCTestCase {
	override func setUp() {
		super.setUp()
	
		FailedAdsStorager.default.clean()
	}
	
	override func tearDown() {
		super.tearDown()
	}
	
	func testDefaultEmptyArrayOfAdFailedDates() {
		let storager = FailedAdsStorager.default
		let count = storager.getArrayOfAdFailedDates().count
		XCTAssertEqual(count, 0)
	}
	
	func testAddingAdFailedAds() {
		let storager = FailedAdsStorager.default
		storager.storeAdFailedDate(Date())
		let count = storager.getArrayOfAdFailedDates().count
		XCTAssertEqual(count, 1)
	}
	
	func testClean() {
		let storager = FailedAdsStorager.default
		storager.storeAdFailedDate(Date())
		storager.clean()
		let count = storager.getArrayOfAdFailedDates().count
		XCTAssertEqual(count, 0)
	}
}
