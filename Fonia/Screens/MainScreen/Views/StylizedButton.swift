//
//  StylizedButton.swift
//  Fonia
//
//  Created by Anton Shcherbach on 16/04/2021.
//

import UIKit

struct ButtonStyle {
	let cornerRadius: CGFloat
	let backgroundColor: UIColor
	let textColor: UIColor
	let text: String
	let font: UIFont?
	
	let leftIcon: UIImage?
	let rightIcon: UIImage?
}

private struct StylizedButtonUX {
	static let iconSize = CGSize(width: 26.0, height: 26.0)
	static let leftIconInset: CGFloat = 16.0
	static let rightIconInset: CGFloat = 14.0
}

class StylizedButton: UIControl {
	private lazy var leftIconImageView: UIImageView = {
		let leftIconImageView = UIImageView()
		leftIconImageView.contentMode = .scaleAspectFit
		addSubview(leftIconImageView)
		return leftIconImageView
	}()

	private lazy var detailDisclosureImageView: UIImageView = {
		let detailDisclosureImageView = UIImageView()
		detailDisclosureImageView.contentMode = .scaleAspectFit
		addSubview(detailDisclosureImageView)
		return detailDisclosureImageView
	}()
	
	private(set) lazy var titleLabel: UILabel = {
		let titleLabel = UILabel()
		titleLabel.textAlignment = .center
		addSubview(titleLabel)
		return titleLabel
	}()
	
	private(set) var style: ButtonStyle? {
		didSet {
			guard let style = style else { return }
			
			layer.cornerRadius = style.cornerRadius
			backgroundColor = style.backgroundColor
			
			leftIconImageView.image = style.leftIcon
			detailDisclosureImageView.image = style.rightIcon
			
			titleLabel.textColor = style.textColor
			titleLabel.text = style.text
			titleLabel.font = style.font
		}
	}
	
	func setButtonStyle(_ style: ButtonStyle?) {
		self.style = style
	}
	
	override var isHighlighted: Bool {
		didSet {
			if isHighlighted {
				alpha = 0.5
			} else {
				alpha = 1.0
			}
		}
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		leftIconImageView.frame = CGRect(x: StylizedButtonUX.leftIconInset,
										 y: bounds.height/2.0-StylizedButtonUX.iconSize.height/2.0,
										 width: StylizedButtonUX.iconSize.width,
										 height: StylizedButtonUX.iconSize.height)
		
		detailDisclosureImageView.frame = CGRect(x: bounds.width-StylizedButtonUX.iconSize.width-StylizedButtonUX.rightIconInset,
												 y: bounds.height/2.0-StylizedButtonUX.iconSize.height/2.0,
												 width: StylizedButtonUX.iconSize.width,
												 height: StylizedButtonUX.iconSize.height)
		
		titleLabel.frame = CGRect(x: leftIconImageView.frame.maxX,
								  y: 0,
								  width: detailDisclosureImageView.frame.minX-leftIconImageView.frame.maxX,
								  height: bounds.height)
	}
}
