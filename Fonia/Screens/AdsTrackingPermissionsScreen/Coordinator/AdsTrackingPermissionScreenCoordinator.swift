//
//  AdsTrackingPermissionVC.swift
//  Fonia
//
//  Created by Anton Shcherbach on 06/04/2021.
//

import UIKit

class AdsTrackingPermissionScreenCoordinator: BaseCoordinator {
	let navigationController: UINavigationController

	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	override func start() {
		navigationController.setNavigationBarHidden(true, animated: true)

		guard let viewController = UIStoryboard(name: "AdsTracking", bundle: nil).instantiateInitialViewController() as? AdsTrackingPermissionVC  else { return }
		viewController.delegate = self
		viewController.modalPresentationStyle = .overFullScreen
		navigationController.pushViewController(viewController, animated: true)
	}
}

extension AdsTrackingPermissionScreenCoordinator: AdsTrackingPermissionDelegate {
	func showAppSettings() {
		UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
	}

	func permissionsAccepted() {
		isCompleted?()
	}
}
