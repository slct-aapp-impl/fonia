//
//  AutorizationStorager.swift
//  Fonia
//
//  Created by Anton Shcherbach on 22/04/2021.
//

import Foundation

class AuthorizationStorager {
	static let `default` = AuthorizationStorager()
	
	private let kPhoneNumber = "kPhoneNumer"

	private let storage: UserDefaults
		
	init(storage: UserDefaults = UserDefaults.standard) {
		self.storage = storage
	}
	
	func store(_ phoneNumber: String) {
		storage.set(phoneNumber, forKey: kPhoneNumber)
		storage.synchronize()
	}
	
	func getPhoneNumber() -> String? {
		return storage.string(forKey: kPhoneNumber)
	}
	
	func isUserAuthorized() -> Bool {
		return getPhoneNumber() != nil
	}
	
	func clean() {
		storage.set(nil, forKey: kPhoneNumber)
		storage.synchronize()
	}
}
