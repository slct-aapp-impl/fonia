//
//  MainScreenCoordinator.swift
//  Fonia
//
//  Created by Anton Scherbach on 25.03.21.
//

import UIKit

class MainScreenCoordinator: BaseCoordinator {
	let navigationController: UINavigationController

	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	override func start() {
		navigationController.setNavigationBarHidden(true, animated: true)

		guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? MainVC  else { return }
		viewController.modalPresentationStyle = .overFullScreen
		navigationController.viewControllers = [viewController]
	}
}
