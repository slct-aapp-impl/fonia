//
//  UIColor+Extensions.swift
//  Fonia
//
//  Created by Anton Shcherbach on 11/06/2021.
//

import UIKit

extension UIColor {
	static let darkCharcoal = UIColor(red   : 51/255,
									  green : 51/255,
									  blue  : 51/255,
									  alpha : 1.0)
	
	static let blueCharcoal = UIColor(red   : 35/255,
									  green : 36/255,
									  blue  : 34/255,
									  alpha : 1.0)

}
