//
//  AuthorizationVC.swift
//  Fonia
//
//  Created by Anton Shcherbach on 20/04/2021.
//

import UIKit
import Promises
import MBProgressHUD
import PhoneNumberKit

protocol AuthorizationScreenDelegate: AnyObject {
	func showErrorScreen()
	func showSuccessScreen()
}

class AuthorizationVC: UIViewController {
	weak var delegate: AuthorizationScreenDelegate?
	
	private var authorizationPresenter: AuthorizationPresenter!

	@IBOutlet private weak var titleLabel: UILabel!
	
	@IBOutlet private weak var backButton: UIButton!
	@IBOutlet private weak var inputPhoneNumberTextField: PolandPhoneNumberTextField! {
		didSet {
			inputPhoneNumberTextField.withPrefix = true
			inputPhoneNumberTextField.withExamplePlaceholder = true
			
			configureTextFieldUI(inputPhoneNumberTextField)
		}
	}
	
	@IBOutlet private weak var inputCodeTextField: UITextField! {
		didSet {
			inputCodeTextField.textContentType = .oneTimeCode
			configureTextFieldUI(inputCodeTextField)
		}
	}
	
	@IBOutlet private weak var inputActionButton: UIButton! {
		didSet {
			inputActionButton.layer.cornerRadius = 6.0
		}
	}
	
	@IBOutlet private weak var additionalActionButton: UIButton! {
		didSet {
			additionalActionButton.layer.cornerRadius = 6.0
		}
	}
		
	// MARK: - Controller Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
		
		authorizationPresenter = AuthorizationPresenter(authorizationModel: nil,
														viewController: self,
														state: .verifyPhoneNumber)
		addTapGestureHandler()
		registerToAuthorizationLocalPushReminder()
	}
	
	// MARK: - Helper Methods
	
	private func addTapGestureHandler() {
		let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
		self.view.addGestureRecognizer(tapGesture)
	}
	
	private func configureTextFieldUI(_ textField: UITextField) {
		textField.layer.borderWidth = 2.0
		textField.layer.borderColor = UIColor.darkCharcoal.cgColor
		textField.layer.cornerRadius = 10.0
		textField.layer.masksToBounds = true
	}
	
	// MARK: - Authorization Reminder
	
	private let notificationAuthorizationReminderIdentifier = "auhtorization_reminder_local_notification"
	
	private func registerToAuthorizationLocalPushReminder() {
		unregisterFromAuthorizationLocalPushReminder()
		
		let content = UNMutableNotificationContent()
		content.title = NSLocalizedString("push_notification_authorization_reminder_title", comment: "")
		content.body = NSLocalizedString("push_notification_authorization_reminder_body", comment: "")
		content.badge = 1
		let trigger = UNTimeIntervalNotificationTrigger(timeInterval: Constants.authorizationReminderTimeInterval, repeats: true)

		let uuid = notificationAuthorizationReminderIdentifier
		let request = UNNotificationRequest(identifier: uuid, content: content, trigger: trigger)

		UNUserNotificationCenter.current().add(request) { (error) in print("Error")}
	}

	private func unregisterFromAuthorizationLocalPushReminder() {
		UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [notificationAuthorizationReminderIdentifier])
		UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [notificationAuthorizationReminderIdentifier])
	}
	
	// MARK: - Actions
	
	@IBAction private func inputCodeTextFieldEditingChanged(_ sender: Any) {
		guard let textField = sender as? UITextField else { return }
		authorizationPresenter.setTextFieldValue(textField.text ?? "")
	}

	@IBAction private func inputPhoneNumberTextFieldEditingChanged(_ sender: Any) {
		guard let textField = sender as? PhoneNumberTextField else { return }
		authorizationPresenter.setTextFieldValue(textField.phoneNumber?.numberString ?? "")
	}
	
	@IBAction private func inputActionButtonTouched(_ sender: Any) {
		authorizationPresenter.actionForCurrentState()
		hideKeyboard()
	}
	
	@IBAction private func additionActionButtonTouched(_ sender: Any) {
		authorizationPresenter.additionalActionForCurrentState()
		hideKeyboard()
	}
	
	@IBAction private func backButtonTouched(_ sender: Any) {
		authorizationPresenter.backActionForCurrentState()
		hideKeyboard()
	}
	
	@objc
	private func hideKeyboard() {
		view.endEditing(true)
	}
}

extension AuthorizationVC: AuthorizationViewProtocol {
	func success(_ phoneNumber: String) {
		hideKeyboard()
		unregisterFromAuthorizationLocalPushReminder()
		
		AuthorizationStorager.default.store(phoneNumber)
		delegate?.showSuccessScreen()
	}
	
	func reactOn(error: Error) {
		switch error {
		case AuthorizationError.invalidPhoneNumber:
			try? AlertFactory.present(alertType: .error, message: AuthorizationError.invalidPhoneNumber.localizedDescription, on: self)
		case AuthorizationError.invalidCode:
			hideKeyboard()
			delegate?.showErrorScreen()
		
			DispatchQueue.main.asyncAfter(deadline: .now()+0.3) { // should reset after small delay
				self.authorizationPresenter.reset()
			}
		default:
			try? AlertFactory.present(alertType: .error, message: error.localizedDescription, on: self)
		}
	}
	
	func disableUI(_ disable: Bool) {
		if disable {
			MBProgressHUD.showAdded(to: self.view, animated: true)
		} else {
			MBProgressHUD.hide(for: self.view, animated: true)
		}
	}

	func setAuthorizationViewState(_ state: AuthorizationViewState) {
		switch state {
		case .verifyPhoneNumber:
			inputPhoneNumberTextField.alpha = 1.0
			inputPhoneNumberTextField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("authorization_main_verify_phone_number_textfield_placeholder", comment: ""),
																				 attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkCharcoal])
			inputPhoneNumberTextField.text = ""
			
			inputCodeTextField.alpha = 0.0
			
			titleLabel.text = NSLocalizedString("authorization_main_verify_phone_number_title", comment: "")
			
			inputActionButton.setTitle(NSLocalizedString("authorization_main_verify_phone_number_button_send_code", comment: ""), for: .normal)
			additionalActionButton.setTitle(NSLocalizedString("authorization_main_verify_phone_number_button_first_login", comment: ""), for: .normal)
		case .verifySMSCode:
			inputPhoneNumberTextField.alpha = 0.0
			
			inputCodeTextField.attributedPlaceholder = NSAttributedString(string: NSLocalizedString("authorization_main_verify_code_textfield_placeholder", comment: ""),
																		  attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkCharcoal])
			inputCodeTextField.text = ""
			inputCodeTextField.alpha = 1.0
			inputCodeTextField.keyboardType = .decimalPad

			titleLabel.text = NSLocalizedString("authorization_main_verify_code_title", comment: "")

			inputActionButton.setTitle(NSLocalizedString("authorization_main_verify_code_button_verify", comment: ""), for: .normal)
			additionalActionButton.setTitle(NSLocalizedString("authorization_main_verify_code_button_send_again", comment: ""), for: .normal)
		}
	}
	
	func openInAppWebURL(_ url: URL) {
		WebVC.present(with: url, from: self)
	}
}
